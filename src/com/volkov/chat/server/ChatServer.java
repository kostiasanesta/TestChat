package com.volkov.chat.server;


import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

public class ChatServer {
    private ServerSocket serverSocket;
    public static List<ThreadServer> threads;


    public ChatServer() {
        threads = new ArrayList<>();
    }

    public static void main(String[] arr) {
        ChatServer chatServer = new ChatServer();
        chatServer.runTheApp();
    }

    private void runTheApp() {
        try {
            serverSocket = new ServerSocket(8030);
            System.out.println("initialize");
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        while (true){
            try {
               Socket socket= serverSocket.accept();
                ThreadServer threadServer = new ThreadServer(socket);
                if(threads.size()>0){
                    for (ThreadServer thread:threads) {
                        String connect = socket.getInetAddress().getAddress()+" connected";
                        System.out.println(connect);
                        thread.getPrintWriter().println(connect);
                    }
                }
                threads.add(threadServer);
                threadServer.start();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    }


}

package com.volkov.app;


import java.net.InetAddress;
import java.net.UnknownHostException;

public class AdressSercher {

    private InetAddress myIP;
    private InetAddress stormnetIP;

    public static void main(String[] arr){
        AdressSercher app = new AdressSercher();
        app.runTheApp();


    }

    private void runTheApp() {
        try {
            myIP = InetAddress.getLocalHost();
            System.out.println("My IP "+myIP.getHostAddress());
            stormnetIP=InetAddress.getByName("www.it-courses.by");
            System.out.println("Stormnet IP "+stormnetIP.getHostAddress());

        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
    }


}
